package iti.pingouin.retrofit.common

import iti.pingouin.retrofit.`interface`.RetrofitService
import iti.pingouin.retrofit.retrofit.RetrofitClient

object Common {
    private var BASE_URL = "http://simplifiedcoding.net/demos/"
    val retrofitService: RetrofitService
        get() = RetrofitClient.getClient(BASE_URL).create(RetrofitService::class.java)
}