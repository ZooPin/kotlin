package iti.pingouin.login

class Constant {
    companion object {
        const val ERROR_EMAIL = "Please enter a valid Email."
        const val ERROR_PASSWORD = "Please enter a password."
        const val ERROR_SAVED = "Error cannot saved information"

        const val PASSWORD = "password"
        const val EMAIL = "email"
        const val PREFERENCE_NAME = "user_data"
        const val INFORMATION_SAVED = "Information saved"
    }
}