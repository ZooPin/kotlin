package iti.pingouin.roomdatabase

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import androidx.recyclerview.widget.RecyclerView

class FriendListAdapter( friendList: List<Friend> ) :
    RecyclerView.Adapter<FriendListAdapter.ViewHolder>() {

    private var dataset: MutableList<Friend> = friendList.toMutableList()

    override fun getItemCount() = dataset.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater
            .from(parent.context)
            .inflate(R.layout.adapter_friend_list, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val friend = dataset[position]
        holder.tvName.setText( friend.firstName )
        holder.tvRating.setText( friend.rating.toString() )

        holder.bEdit.setOnClickListener {
            val editFriend =
                Friend(
                    uid = dataset[position].uid,
                    firstName = holder.tvName.text.toString(),
                    rating = holder.tvRating.text.toString().toInt()
                )
            eventListener?.onFriendEdit(editFriend)
        }

        holder.bRemove.setOnClickListener {
            eventListener?.onFriendDelete(dataset[position])
        }
    }

    fun updateData( friendList: List<Friend> ) {
        dataset.clear()
        dataset.addAll(friendList)
        notifyDataSetChanged()
    }

    private var eventListener: EventListener? = null
    fun setEventListener(eventListener: EventListener) {
        this.eventListener = eventListener
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val tvName = view.findViewById<EditText>(R.id.inputName)
        val tvRating = view.findViewById<EditText>(R.id.inputRating)
        val bEdit = view.findViewById<Button>(R.id.buttonEdit)
        val bRemove = view.findViewById<Button>(R.id.buttonRemove)
    }

    interface EventListener {
        fun onFriendEdit(friend: Friend)
        fun onFriendDelete(friend: Friend)
    }
}